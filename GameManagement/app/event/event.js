﻿(function () {
    'use strict';

    // Controller name is handy for logging
    var controllerId = 'event';

    // Define the controller on the module.
    // Inject the dependencies. 
    // Point to the controller definition function.
    angular.module('app').controller(controllerId,
        ['bootstrap.dialog', 'common', 'config', 'datacontext', '$scope', event]);

    function event(bsDialog, common, config, datacontext, $scope) {
        // Using 'Controller As' syntax, so we event this to the vm variable (for viewmodel).
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);


        // Bindable properties and functions are placed on vm.
        vm.altInputFormats = ['M!/d!/yyyy', 'M!/d!/yy']
        vm.datepickerOpts = { showWeeks: false };
        vm.eventDetails;
        //vm.eventDetails = datacontext.event.create();
        vm.eventForm;
        vm.onCancel = _onCancel;
        vm.onCreate = _onCreate;
        vm.onDelete = _onDelete;
        vm.onSelectEvent = _onSelectEvent;
        vm.onSubmit = _onSubmit;
        vm.selectedEvent = {};
        vm.title = 'Event';


        activate();

        function activate() {
            onDestroy();

            common.activateController([getEvents(), getSports(), getTv(), getVenues()], controllerId)
                .then(function () { log('Activated event View', null, false); })
            ;
        }


        function cancelDialog(){
            return bsDialog.confirmationDialog('Are you sure you want to cancel?', 'Changes will not be saved.');
        }

        function getEvents(forceRefresh) {
            return datacontext.event.getAll(forceRefresh)
                .then(function (data) {
                    vm.events = data;
                    return data;
                });
        }

        function getSports(forceRefresh) {
            return datacontext.sport.getAll(forceRefresh)
                .then(function (data) {
                    vm.sports = data;
                    return data;
                });
        }

        function getTv(forceRefresh) {
            return datacontext.tv.getAll(forceRefresh)
                .then(function (data) {
                    vm.tv = data;
                    return data;
                });
        }

        function getVenues(forceRefresh) {
            return datacontext.venue.getAll(forceRefresh)
                .then(function (data) {
                    vm.venues = data;
                    return data;
                });
        }

        function _onCancel() {
            if (vm.eventDetails && vm.eventDetails.entityAspect.entityState.isModified()) {
                cancelDialog()
                    .then(clearForm, null);
            }
            else {
                clearForm();
            }

            function clearForm() { 
                vm.selectedEvent = null;
                vm.eventDetails = null;
                vm.eventForm.$setPristine();
                vm.eventForm.$setUntouched();
                datacontext.cancel(false);
            }
        };

        function _onCreate() {
            if (vm.eventDetails && vm.eventDetails.entityAspect.entityState.isModified()) {
                cancelDialog()
                    .then(function () {
                        datacontext.cancel(false);
                        vm.selectedEvent = null;
                        vm.eventDetails = datacontext.event.create();
                    });
            }
            else {
                vm.selectedEvent = null;
                vm.eventDetails = datacontext.event.create();
            }
        }

        function _onDelete() {
            bsDialog.confirmationDialog('Are you sure you want to delete this event?', vm.eventDetails.event_name)
                .then(function () {
                    datacontext.markDeleted(vm.eventDetails);
                    save();
                });
        }

        function onDestroy() {
            $scope.$on('$destroy', function () {
                datacontext.cancel(false);
            });
        }

        function _onSelectEvent() {
            if (vm.eventDetails && vm.eventDetails.entityAspect.entityState.isAddedModifiedOrDeleted()) {
                cancelDialog()
                    .then(function () {
                        datacontext.cancel(false);
                        vm.eventDetails = vm.selectedEvent;
                    }, function () {
                        vm.selectedEvent = vm.eventDetails;
                    });
            }
            else {
                vm.eventDetails = vm.selectedEvent;
            }
        }

        function _onSubmit() {
            if (vm.eventForm.$valid) {
                save();
            }
        }

        function save() {
            return datacontext.save()
                    .then(function (saveResult) {
                        getEvents();
                        vm.eventForm.$setPristine();
                        vm.eventForm.$setUntouched();
                        vm.selectedEvent = null;
                        vm.eventDetails = null;
                    }, function (error) {
                        datacontext.cancel();
                    });
        }


    }
})();
