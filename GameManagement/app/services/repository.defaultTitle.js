﻿(function () {
    'use strict';

    var serviceId = 'repository.defaulttitle';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryDefaultTitle]);

    function RepositoryDefaultTitle(model, AbstractRepository) {
        var entityName = model.entityNames.defaultTitle;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'preference, position';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote, staffId, sportId) {
            var self = this;
            var defaultTitles = [];

            var predicate = Predicate
                .create('staff_ID', '==', staffId)
                .and('sport_ID', '==', sportId);

            if (!forceRemote && self._areItemsLoaded()) {
                defaultTitles = self._getAllLocal(entityName, orderBy, predicate);
                return self.$q.when(defaultTitles);
            }

            return EntityQuery.from('Default_title')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                defaultTitles = data.results;
                self.log('Retrieved [Default Titles] from remote data source', defaultTitles.length, false);
                return defaultTitles;
            }
        }


    }
})();