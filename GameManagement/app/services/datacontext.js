(function () {
    'use strict';

    var serviceId = 'datacontext';
    angular.module('app').factory(serviceId,
        ['common', 'entityManagerFactory', 'model', 'repositories', datacontext]);

    function datacontext(common, emFactory, model, repositories) {
        var entityNames = model.entityNames;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(serviceId);
        var logError = getLogFn(serviceId, 'error');
        var logSuccess = getLogFn(serviceId, 'success');
        var manager = emFactory.newManager();
        var primePromise;
        var repoNames = ['assign', 'event', 'defaultTitle', 'official', 'officialTitle', 'payRate', 'position', 'sport', 'staff', 'title', 'tv', 'venue'];
        var $q = common.$q;

        var service = {
            cancel: cancel,
            markDeleted: markDeleted,
            prime: prime,
            save: save
            // Repositories to be added on demand:
            //      attendees
            //      lookups
            //      sessions
            //      speakers
        };

        init();

        return service;

        function init() {
            repositories.init(manager);
            defineLazyLoadedRepos();
        }

        function cancel(shouldLog) {
            manager.rejectChanges();
            shouldLog = shouldLog !== undefined ? shouldLog : true;
            logSuccess('Canceled changes', null, shouldLog);
        }

        // Add ES5 property to datacontext for each named repo
        function defineLazyLoadedRepos() {
            repoNames.forEach(function (name) {
                Object.defineProperty(service, name, {
                    configurable: true, // will redefine this property once
                    get: function () {
                        // The 1st time the repo is request via this property, 
                        // we ask the repositories for it (which will inject it).
                        var repo = repositories.getRepo(name);
                        // Rewrite this property to always return this repo;
                        // no longer redefinable
                        Object.defineProperty(service, name, {
                            value: repo,
                            configurable: false,
                            enumerable: true
                        });
                        return repo;
                    }
                });
            });
        }

        function markDeleted(entity) {
            return entity.entityAspect.setDeleted();
        }

        function prime() {
            if (primePromise) return primePromise;

            primePromise = $q.all([service.position.getAll()])
                .then(extendMetadata)
                .then(success);
            return primePromise;

            function success() {
                log('Primed the data', null, false);
            }

            function extendMetadata() {
                var metadataStore = manager.metadataStore;
                var types = metadataStore.getEntityTypes();
                types.forEach(function (type) {
                    if (type instanceof breeze.EntityType) {
                        set(type.shortName, type);
                    }
                });


                function set(resourceName, entityName) {
                    metadataStore.setEntityTypeForResourceName(resourceName, entityName);
                }
            }
        }

        function save() {
            return manager.saveChanges()
                .then(saveSucceeded, saveFailed);

            function saveSucceeded(result){
                logSuccess('Save successful.', result, true);
            }

            function saveFailed(error) {
                var msg = 'Error saving: ' + breeze.saveErrorMessageService.getErrorMessage(error);
                error.message = msg;
                logError(msg, error);
                throw error;
            }
        }
    }
})();