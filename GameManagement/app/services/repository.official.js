﻿(function () {
    'use strict';

    var serviceId = 'repository.official';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositoryOfficial]);

    function RepositoryOfficial(model, AbstractRepository) {
        var entityName = model.entityNames.official;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'lname';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
            this.getJoinedWithTitle = getJoinedWithTitle;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
            // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var officials = [];

            if (!forceRemote && self._areItemsLoaded()) {
                officials = self._getAllLocal(entityName, orderBy);
                return self.$q.when(officials);
            }

            return EntityQuery.from('Official')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                officials = data.results;
                self.log('Retrieved [officials] from remote data source', officials.length, false);
                return officials;
            }
        }

        function getJoinedWithTitle(forceRemote, sportId) {
            var self = this;
            var officials = [];

            if (!forceRemote && self._areItemsLoaded()) {
                officials = self._getAllLocal(entityName, orderBy);
                return self.$q.when(officials);
            }

            return EntityQuery.from('OfficialJoinWithTitle')
                .orderBy(orderBy)
                .toType(entityName)
                .withParameters({sportId: sportId})
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
                self._areItemsLoaded(true);
                officials = data.results;
                self.log('Retrieved [officials] from remote data source', officials.length, false);
                return officials;
            }
        }


    }
})();