﻿(function () {
    'use strict';

    var serviceId = 'repository.sport';
    angular.module('app').factory(serviceId,
        ['model', 'repository.abstract', RepositorySport]);

    function RepositorySport(model, AbstractRepository) {
        var entityName = model.entityNames.sport;
        var EntityQuery = breeze.EntityQuery;
        var orderBy = 'id';
        var Predicate = breeze.Predicate;

        function Ctor(mgr) {
            this.serviceId = serviceId;
            this.entityName = entityName;
            this.manager = mgr;
            // Exposed data access functions
            this.getAllLocal = getAllLocal;
            this.getAll = getAll;
        }

        AbstractRepository.extend(Ctor);

        return Ctor;

        // Formerly known as datacontext.getLocal()
        function getAllLocal() {
            var self = this;
           // var predicate = Predicate.create('isSpeaker', '==', true);
            return self._getAllLocal(entityName, orderBy);
        }

        // Formerly known as datacontext.getSpeakerPartials()
        function getAll(forceRemote) {
            var self = this;
            var sports = [];

            if (!forceRemote && self._areItemsLoaded()) {
                sports = self._getAllLocal(entityName, orderBy);
                return self.$q.when(sports);
            }

            return EntityQuery.from('Sport')
                .orderBy(orderBy)
                .toType(entityName)
                .using(self.manager).execute()
                .then(querySucceeded, self._queryFailed);

            function querySucceeded(data) {
            	self._areItemsLoaded(true);
                sports = data.results;
                self.log('Retrieved [sports] from remote data source', sports.length, false);
                return sports;
            }
        }


    }
})();