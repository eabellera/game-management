﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameManagement.Tests
{
    [TestClass()]
    public class ConvertToUtcTests
    {
        [TestMethod()]
        public void ConvertTest()
        {
            ConvertToUtc c = new ConvertToUtc();
            int result = c.Convert();

            Assert.AreEqual(511, result);
        }
    }
}